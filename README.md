What's this?
=============
Ａ simple practice following [custom-views] in CodeLabs.

Additional knowledge required
=============
* Android Extension
* Mathematical formula like sin and cos to get opposite(對邊) and adjacent(鄰邊)

[custom-views]:https://developer.android.com/codelabs/advanced-android-kotlin-training-custom-views

