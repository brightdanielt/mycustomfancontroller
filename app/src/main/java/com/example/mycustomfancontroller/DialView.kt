package com.example.mycustomfancontroller

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.view.AccessibilityDelegateCompat
import androidx.core.view.ViewCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin

private const val RADIUS_OFFSET_LABEL = 45
private const val RADIUS_OFFSET_INDICATOR = -45

class DialView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
        textSize = 55.0f
        typeface = Typeface.create("", Typeface.BOLD)
    }

    private var radius = 0.0f
    private var fanSpeed = FanSpeed.OFF

    //Point at where to draw label and indicator.
    private val pointPosition = PointF(0.0f, 0.0f)

    private val fanSpeedOffColor: Int
    private val fanSpeedLowColor: Int
    private val fanSpeedMediumColor: Int
    private val fanSpeedHighColor: Int

    init {
        //Ensure this view is clickable.
        isClickable = true

        //Get attrs from xml.
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.DialView)
        fanSpeedOffColor = typedArray.getColor(R.styleable.DialView_fanColorOff, 0)
        fanSpeedLowColor = typedArray.getColor(R.styleable.DialView_fanColorLow, 0)
        fanSpeedMediumColor = typedArray.getColor(R.styleable.DialView_fanColorMedium, 0)
        fanSpeedHighColor = typedArray.getColor(R.styleable.DialView_fanColorHigh, 0)
        typedArray.recycle()

        updateContentDescription()

        ViewCompat.setAccessibilityDelegate(this, object : AccessibilityDelegateCompat() {
            override fun onInitializeAccessibilityNodeInfo(
                host: View?, info: AccessibilityNodeInfoCompat
            ) {
                super.onInitializeAccessibilityNodeInfo(host, info)
                val customClick = AccessibilityNodeInfoCompat.AccessibilityActionCompat(
                    AccessibilityNodeInfoCompat.ACTION_CLICK,
                    context.getString(if (fanSpeed != FanSpeed.HIGH) R.string.change else R.string.reset)
                )
                info.addAction(customClick)
            }
        })
    }

    override fun performClick(): Boolean {
        if (super.performClick()) return true

        fanSpeed = fanSpeed.next()
        updateContentDescription()
        invalidate()
        return true
    }

    /**
     * Update the radius as long as this view's size changed.
     * Choose the smaller of the width and height to calculate the radius.
     * */
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        radius = (min(w, h) / 2 * 0.8).toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        //Assign color of the paint depends on fan's speed
        paint.color = when (fanSpeed) {
            FanSpeed.OFF -> fanSpeedOffColor
            FanSpeed.LOW -> fanSpeedLowColor
            FanSpeed.MEDIUM -> fanSpeedMediumColor
            FanSpeed.HIGH -> fanSpeedHighColor
        }

        //Draw the big circle
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radius, paint)

        //Draw the indicator
        val indicatorRadius = radius + RADIUS_OFFSET_INDICATOR
        pointPosition.computeXYForSpeed(fanSpeed, indicatorRadius)
        paint.color = Color.BLACK
        canvas.drawCircle(pointPosition.x, pointPosition.y, radius / 10, paint)

        //Draw all the speed label
        val labelRadius = radius + RADIUS_OFFSET_LABEL
        for (speed in FanSpeed.values()) {
            pointPosition.computeXYForSpeed(speed, labelRadius)
            val text = resources.getString(speed.label)
            canvas.drawText(text, pointPosition.x, pointPosition.y, paint)
        }
    }

    /**
     * Compute X/Y-coordinates for a label or indicator,
     * given the FanSpeed and radius where thr label should be drawn.
     * */
    private fun PointF.computeXYForSpeed(pos: FanSpeed, radius: Float) {
        val startAngle = 0
        val angle = startAngle + pos.ordinal * (Math.PI / FanSpeed.values().size)
        x = (radius * cos(angle.toFloat())) + width / 2
        y = (radius * sin(angle.toFloat())) + height / 2
    }

    private fun updateContentDescription() {
        contentDescription = resources.getString(fanSpeed.label)
    }

}